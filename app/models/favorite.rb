class Favorite < ActiveRecord::Base
  serialize :image
  validates_uniqueness_of :image
end
