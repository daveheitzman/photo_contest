class Game < ActiveRecord::Base
  
  serialize :pairs, Array
  serialize :images, Array
  serialize :votes, Array

  
  after_initialize do
    self.pairs ||= []
    self.images ||= []
    self.votes ||= []
    self.choice ||= 0
  end 

  def winners
    images.map{|i| i if i[:winner]==1 }.compact
  end 
  
  #precious
  def over?
    self[:round] == 0
  end 
  
  def choice_number
    self[:choice]
  end 

  def choice_number=(n)
    self[:choice]=n
  end 
  
end
