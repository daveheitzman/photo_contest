class FavoritesController < ApplicationController
  def index
    @favorites=Favorite.all
  end 
  
  def create 
    Favorite.create(params[:favorite])
  end 

end
