require 'rubygems'

class GamesController < ApplicationController

  include PhotoApi
  include RandomTags
  
  def start_with_user_tags
    reset_game
    @possible_tags = possible_tags.uniq
    render 'start_with_user_tags'
  end 
  
  def submit_user_tag
    #to be used only as xhr
    session[:user_tags] ||= []
    session[:user_tags] << params[:commit]
    tags=session[:user_tags].uniq.map{|t| "<span>" + t + "&nbsp;</span>"  }.join("") 
    
    respond_to do |format|
        format.js { render :text=>"reload_tag_area('" + tags + "');"  } 
    end 
    return
  end 

  def start #game
    reset_game
    new_round
    play
  end 

  def play
    #play is called upon each vote for a photo. 
    
    @game ||= Game.find(session[:game][:game_id])
    if params[:vote]
      @game.images[params[:vote].to_i][:winner] = 1 
      @game.pairs.pop
      @game.save
    end 

    if @game.pairs.empty?
      if @game.winners.size == 1 
        #end of game. 
        @game_over=@game.winners.first
        post_winners
        post_done
      else
        new_round
      end 
    end 

    @game.choice_number +=1
    @game.save
    @tup=@game.pairs.last  
    @leaderboard=@game.images.map{|i| i if i[:winner]==1 }.compact
    render 'show'
  end 

    def hit_api
      #sends a request to the photos api and dumps the result out the page. mainly for debugging and testing. 
      @req = request.fullpath
      @res = 
      if params[:round]
        self.send( params[:req].to_sym , params[:round] ) 
      else
        self.send params[:req] 
      end 
      #~ @res =  self.send params[:req]
      render 'debug'
    end 
  
  def post_winners
    #at the end of the round, notify the api of the winners of the round .
    winners = []
    @game[:images].each do |im|
      winners<<im['image'] if im[:winner]==1
    end 
    post_store_round_results(@game.round,winners.to_json)
  end 
  
  def new_round
    #tell server results of round that is ending
    if @game.round > 0 
      post_winners
    end
    #set up new round

    if @game.over? 
      @game[:images] = get_images_request 
    else  
      @game[:images] = @game.winners 
    end 
    @game.save
    session[:game][:total_rounds] =4 
    @game[:images].each do |img|
      img[:winner]=0
    end 
    isi=@game[:images].size
    pairs=[]
    (0..isi-1).each do |i| 
      pairs << [i, i+1] if i.even?
    end 
    @game.pairs=pairs.shuffle
    @game.round +=1
    @game.choice_number=0  
    @game.save
    session[:chooses] = @game.pairs.size
    @game
  end 

  
  def reset_game
    @game = params[:id] ? Game.find(params[:id]) : Game.new    
    @game.tags=determine_tags.join(' ')
    @game[:images] =  @yielded_images
    @game.save
    session[:game]={:game_id=>@game.id }
  end 

 
  

  private
  def determine_tags
  # we can't just pick 4 random words and go.
  # it's possible the api won't return enough photos for a given set of words
  # so we start with a long word list (see below). We hit the api with thtat list
  # if we don't get enough photos, we trim the list and try again. As long as you start 
  # with a reasonably sized tag list, it returns soon enough. 
    tags=[]
    tags_to_return=[]
    #this is the implementation of 
logger.info("session tags  " + session[:user_tags].inspect)
logger.info("session tags  " + session[:user_tags].inspect)
logger.info("session tags  " + session[:user_tags].inspect)
logger.info("session tags  " + session[:user_tags].inspect)
logger.info("session tags  " + session[:user_tags].inspect)
    if session[:user_tags] 
      tags=session.delete(:user_tags) 
    else
      # this number is important. if too high, it will create too long a tag list, and
      # make thus fail to find any photos, and thus have to try again. 3 seems about right. 
      # also, the theme generator returns two words, a noun and an adjective, so 3 actually starts you out with 6 
      # words. 
      3.times do 
        tags += RandomTags.pair #rmtg.randthemename.split('_') 
      end   
    end 
    yielded_images=[]
    while yielded_images.size < 16 && tags.length>0
      tags=tags.shuffle 
      tags_to_return=tags.clone  # get them before the pop, in case we're all done.
      post_reset(tags)
      yielded_images=get_images_request 
      tags.pop 
    end   
    @yielded_images=yielded_images
    tags_to_return
  end 
  
  def possible_tags
    pt=[]
    100.times do 
       @possible_tags = pt += RandomTags.pair
    end 
     @possible_tags
  end 
end
