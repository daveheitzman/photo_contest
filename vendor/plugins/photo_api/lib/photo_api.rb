# PhotoApi
require 'open-uri'
require 'json'
require 'net/http'

module PhotoApi

  def url
    PhotoContest::Application::API_URL
  end 
  def key
    PhotoContest::Application::API_KEY
  end 
  
  def safe_send(str='')
    return if str.blank? 
    begin
#logger.info('net '+str)
      JSON.parse open(str).read
    rescue Exception=>e
      e.to_s
    end 
  end 
  
  def safe_post
  end 
  
  def get_default_request
  #GET	/contest/<key> =>  JSON	data
    @response_data =@get_default_request=safe_send( url+key )
  end 
  def get_images_request
  #GET	/contest/<key>/images =>  JSON	data
    @response_data =safe_send url+key+'/images'
  end 
  def get_rounds_request
  #GET	/contest/<key>/round =>  JSON	data
    @response_data =safe_send url+key+'/round'
  end 
  def get_round_request(n)
  #GET	/contest/<key>/round/<n> =>  JSON	data
    @response_data =safe_send url+key+'/round'+"/#{n}"
  end 
  def post_reset(tags=[])
  #POST	/contest/<key>/reset =>  JSON	(OK|Fail)
  #POST	/contest/<key>/reset?tags=bread,cheese =>  JSON	data
    
    @response_data =
    begin
      uri = URI(url+key+'/reset')
      res=
      if ! tags.empty?
        Net::HTTP.post_form(uri, 'tags' => tags.join(",") )
      else
        Net::HTTP.post_form( uri,{} )
      end 
      res.body
    rescue Exception=>e
      {"Exception" => e}
    end 
    @response_data 
  end 

  def post_done
  #POST	/contest/<key>done =>  JSON	(OK|Fail)
    @response_data=
    begin
      uri = URI(url+key+'/done')
      res=  Net::HTTP.post_form( uri, {})
      res.body
    rescue Exception=>e
      {"Exception" => e}
    end 
  end 

  def post_store_round_results(round=0,payload='{}')
  #records results of a round of voting
  #POST	/contest/<key>/round/<n> =>  JSON	(OK|Fail)
  #include multipart/form payload containing winning photo infos [{"flicker_id":"234234"},{}]
    @response_data = @post_store_round_results=
    begin
      uri = URI(url+key+"/round/#{round}")
      res=if ! payload.empty?
        req = Net::HTTP::Post.new(uri.path)
        req.body = "data="+payload.to_s
        req.content_type = 'multipart/form-data'
        Net::HTTP.start(uri.host, uri.port) do |http|
          http.request(req)
        end
      end 
      res.body
    rescue Exception=>e
      {"Exception" => e}
    end 
  end 
  
 

  

  

end

