class CreateGames < ActiveRecord::Migration
  def self.up
    create_table :games do |t|
      t.string :remote_ip 
      t.text :images
      t.text :pairs
      t.text :votes
      t.integer :choice, :default=>0
      t.integer :round, :default=>0
      t.string :tags
      t.timestamps
    end
    create_table :moves do |t|
      t.integer :game_id
      t.string :move
    end 
  end

  def self.down
  end
end
