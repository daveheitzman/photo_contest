class CreateFavorites < ActiveRecord::Migration
  def self.up
    create_table :favorites do |t|
      t.text :image
      t.timestamps
    end
  end

  def self.down
  end
end
